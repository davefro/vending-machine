items = [ {'code':1, 'name':'Coca-Cola', 'price':2},
    {'code':2, 'name':'Chips', 'price':1},
    {'code':3, 'name':'sandwich', 'price':4},
]

is_quit = False
item = ''

while is_quit == False:
    print("Welcome to the vending machine")
    for i in items:
        print(f"Item name: {i['name']} - Price: {i['price']} - code: {i['code']}")

    question = int(input("Enter the code number of the item you want to get: "))
    for i in items:
        if question == i['code']:
            item = i
    if item == '':
        print('INVALID CODE')
    else:
        print(f"Great, {item['name']} will cost you {item['price']} pounds")

        price = int(input(f"Enter {item['price']} pounds to purchase: "))
        if price == item['price']:
            print(f"Thank you for buying here is your {item['name']}")
        else:
            print(f"Please enter only {item['price']} pounds")

    question = input("To quit the machine enter anything and to continue buying enter c: ")
    if question == 'c':
        is_quit = False
    else:
        is_quit = True
    print('')